//
//  ButtonContainer.swift
//  LeRobot
//
//  Created by developer on 29/05/21.
//

import UIKit

final class ButtonContainer: UIView {

    private var stackView = UIStackView()
    private let buttons: [RoundedButton]

    private struct Constant {
        static let spacing: CGFloat = 20
    }

    init(buttons: [RoundedButton]) {
        self.buttons = buttons
        super.init(frame: CGRect(x: .zero, y: .zero, width: .zero, height: .zero))
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        buttons.forEach({ $0.setupLayoutAndAppearance() })
    }

    func applyConstraintTo(_ view: UIView, yAxisAnchor: NSLayoutYAxisAnchor,
                           height: CGFloat = 80, padding: CGFloat = 20) {
        topAnchor.constraint(equalTo: yAxisAnchor, constant: padding).isActive = true
        leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        heightAnchor.constraint(equalToConstant: height).isActive = true
    }

    private func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        buttons.forEach({ stackView.addArrangedSubview($0) })
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = Constant.spacing
        stackView.axis = .horizontal
        stackView
            .leadingAnchor
            .constraint(equalTo: leadingAnchor, constant: Constant.spacing)
            .isActive = true
        stackView
            .trailingAnchor
            .constraint(equalTo: trailingAnchor, constant: -Constant.spacing)
            .isActive = true
        stackView.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
    }
}
