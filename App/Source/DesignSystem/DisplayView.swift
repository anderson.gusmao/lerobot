//
//  DisplayView.swift
//  LeRobot
//
//  Created by developer on 03/06/21.
//

import UIKit

final class DisplayView: UIView {

    private let label = UILabel()

    private struct Constant {
        static let defaultFont = UIFont(name: "HelveticaNeue-Bold", size: 30)
        static let borderWidth: CGFloat = 5
        static let cornerRadiusReference: CGFloat = 15
    }

    init() {
        super.init(frame: CGRect(x: .zero, y: .zero, width: .zero, height: .zero))
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.width / Constant.cornerRadiusReference
    }

    func setValue(_ value: String) {
        label.text = value
    }

    func clean() {
        label.text = .empty
    }

    private func setupView() {
        addSubview(label)
        translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        label.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        label.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        label.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true

        backgroundColor = .lightRose
        layer.borderWidth = Constant.borderWidth
        layer.borderColor = UIColor.amazingRed.cgColor
        layer.cornerRadius = frame.width / Constant.cornerRadiusReference

        label.textColor = .amazingRed
        label.font = Constant.defaultFont
        label.textAlignment = .center
    }
}
