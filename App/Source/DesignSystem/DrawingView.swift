//
//  DrawingView.swift
//  LeRobot
//
//  Created by developer on 28/05/21.
//

import Foundation
import UIKit

final class DrawingView: UIView {

    private var lastPoint = CGPoint.zero
    private var imageView: UIImageView?
    private let size: CGSize

    private struct Constant {
        static let borderWidth: CGFloat = 10
        static let cornerRadiusReference: CGFloat = 15
    }

    let color: UIColor
    let brushWidth: CGFloat
    let opacity: CGFloat
    var image: UIImage? { imageView?.image }

    init(size: CGSize, color: UIColor = .wine, brushWidth: CGFloat = 30, opacity: CGFloat = 1.0) {
        self.color = color
        self.brushWidth = brushWidth
        self.opacity = opacity
        self.size = size
        super.init(frame: CGRect(x: .zero, y: .zero, width: size.width, height: size.height))
        setupAppearance()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        lastPoint = touch.location(in: self)
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        let currentPoint = touch.location(in: self)
        drawLine(from: lastPoint, to: currentPoint)
        lastPoint = currentPoint
    }

    func applyConstraintTo(xAxisAnchor: NSLayoutXAxisAnchor, yAxisAnchor: NSLayoutYAxisAnchor) {
        guard imageView == nil else { return }
        imageView = UIImageView(frame: CGRect(origin: frame.origin, size: size))
        imageView?.alpha = opacity
        guard let imageViewInstance = imageView else { return }
        addSubview(imageViewInstance)
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(equalToConstant: size.width).isActive = true
        heightAnchor.constraint(equalToConstant: size.height).isActive = true
        topAnchor.constraint(equalTo: yAxisAnchor).isActive = true
        centerXAnchor.constraint(equalTo: xAxisAnchor).isActive = true
    }

    func eraseImage() {
        imageView?.image = nil
    }

    private func setupAppearance() {
        backgroundColor = .lightRose
        layer.borderWidth = Constant.borderWidth
        layer.borderColor = UIColor.amazingRed.cgColor
        layer.cornerRadius = size.width / Constant.cornerRadiusReference
    }

    private func drawLine(from fromPoint: CGPoint, to toPoint: CGPoint) {
        UIGraphicsBeginImageContext(frame.size)
        guard let context = UIGraphicsGetCurrentContext() else { return }
        imageView?.image?.draw(in: bounds)

        context.move(to: fromPoint)
        context.addLine(to: toPoint)
        context.setLineCap(.round)
        context.setBlendMode(.normal)
        context.setLineWidth(brushWidth)
        context.setStrokeColor(color.cgColor)
        context.strokePath()

        imageView?.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
}
