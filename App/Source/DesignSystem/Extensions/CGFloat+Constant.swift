//
//  CGFloat+Constant.swift
//  LeRobot
//
//  Created by developer on 29/05/21.
//

import UIKit

extension CGFloat {

    static let zero: CGFloat = 0

    func multiplyByTwo() -> CGFloat { self * 2 }
}
