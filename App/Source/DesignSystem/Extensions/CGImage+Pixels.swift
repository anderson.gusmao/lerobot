//
//  CGImage+Pixels.swift
//  LeRobot
//
//  Created by developer on 04/06/21.
//

import Foundation
import UIKit

struct Pixel {
    let red: Int
    let green: Int
    let blue: Int
    let alpha: Int
}

extension CGImage {
    // swiftlint:disable identifier_name
    func pixelFrom(x: Int, y: Int) -> Pixel? {
        guard let pixelData = dataProvider?.data,
              let data = CFDataGetBytePtr(pixelData) else { return nil }

        let pixelInfo = ((width  * y) + x ) * 4
        let red = Int(data[pixelInfo])
        let green = Int(data[(pixelInfo + 1)])
        let blue = Int(data[pixelInfo + 2])
        let alpha = Int(data[pixelInfo + 3])

        return Pixel(red: red, green: green, blue: blue, alpha: alpha)
    }
}
