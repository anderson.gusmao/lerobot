//
//  String+Capitalize.swift
//  LeRobot
//
//  Created by developer on 29/05/21.
//

import Foundation

extension String {

    static let empty = ""

    func capitalizeFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
}
