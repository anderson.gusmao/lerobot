//
//  UIColor+HEX.swift
//  LeRobot
//
//  Created by developer on 29/05/21.
//

import UIKit

extension UIColor {

    static let lightRose = UIColor(rgb: 0xFFF9EC)
    static let amazingRed = UIColor(rgb: 0xFB6376)
    static let maxRose = UIColor(rgb: 0xFCB1A6)
    static let wine = UIColor(rgb: 0x5D2A42)
    static let softRose = UIColor(rgb: 0xFFDCCC)

    convenience init(red: Int, green: Int, blue: Int) {
        guard red >= 0 && red <= 255 && green >= 0 && green <= 255 && blue >= 0 && blue <= 255 else {
            fatalError("Invalid color component.")
        }

        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    convenience init(rgb: Int) {
        self.init(red: (rgb >> 16) & 0xFF, green: (rgb >> 8) & 0xFF, blue: rgb & 0xFF)
    }
}
