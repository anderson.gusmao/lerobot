//
//  FooterView.swift
//  LeRobot
//
//  Created by developer on 03/06/21.
//

import UIKit

final class FooterView: UIView {

    private lazy var labelView: UILabel = {
        let lbl = UILabel()
        lbl.text = "I think this is a..."
        lbl.textColor = .wine
        lbl.font = UIFont(name: "HelveticaNeue-Bold", size: 50)
        lbl.adjustsFontSizeToFitWidth = true
        return lbl
    }()

    private lazy var imageView: UIImageView = {
        let img = UIImageView(image: UIImage(named: "Robot"))
        img.translatesAutoresizingMaskIntoConstraints = false
        img.heightAnchor.constraint(equalToConstant: 50).isActive = true
        img.widthAnchor.constraint(equalToConstant: 50).isActive = true
        return img
    }()

    lazy var displayView: DisplayView = {
        let view = DisplayView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 60).isActive = true
        view.widthAnchor.constraint(equalToConstant: 60).isActive = true
        return view
    }()

    private lazy var horizontalStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 30
        return stack
    }()

    private let verticalStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 10
        stack.alignment = .center
        return stack
    }()

    init() {
        super.init(frame: CGRect(x: .zero, y: .zero, width: .zero, height: .zero))
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func applyConstraintTo(_ view: UIView, yAxisAnchor: NSLayoutYAxisAnchor, padding: CGFloat = 20) {
        topAnchor.constraint(greaterThanOrEqualTo: yAxisAnchor, constant: padding).isActive = true
        leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: padding).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -padding).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -padding.multiplyByTwo()).isActive = true
    }

    private func setupView() {
        horizontalStackView.addArrangedSubview(imageView)
        horizontalStackView.addArrangedSubview(labelView)
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(verticalStackView)
        verticalStackView.constraintToEdges(self)
        verticalStackView.addArrangedSubview(horizontalStackView)
        verticalStackView.addArrangedSubview(displayView)
    }
}
