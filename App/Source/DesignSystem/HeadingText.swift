//
//  HeadingText.swift
//  LeRobot
//
//  Created by developer on 03/06/21.
//

import UIKit

final class HeadingText: UILabel {

    private struct Constant {
        static let defaultFont = UIFont(name: "HelveticaNeue-Bold", size: 50)
    }

    init(text: String) {
        super.init(frame: CGRect(x: .zero, y: .zero, width: .zero, height: .zero))
        setupViewAppearance()
        self.text = text
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func applyConstraintTo(_ view: UIView, height: CGFloat = 60, padding: CGFloat = 20) {
        translatesAutoresizingMaskIntoConstraints = false
        leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: padding).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -padding).isActive = true
        topAnchor.constraint(equalTo: view.topAnchor, constant: padding).isActive = true
        heightAnchor.constraint(equalToConstant: height).isActive = true
    }

    private func setupViewAppearance() {
        font = Constant.defaultFont
        textColor = .wine
        adjustsFontSizeToFitWidth = true
    }
}
