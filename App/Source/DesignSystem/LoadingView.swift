//
//  LoadingView.swift
//  LeRobot
//
//  Created by developer on 05/06/21.
//

import UIKit

final class LoadingView: UIView {

    private let activityIndicator = UIActivityIndicatorView()

    init() {
        super.init(frame: CGRect(x: .zero, y: .zero, width: .zero, height: .zero))
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func applyConstraintsTo(_ view: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        constraintToEdges(view)
    }

    private func setupView() {
        backgroundColor = .black.withAlphaComponent(0.7)
        addSubview(activityIndicator)
        activityIndicator.startAnimating()
        activityIndicator.style = .large
        activityIndicator.color = .white
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
}
