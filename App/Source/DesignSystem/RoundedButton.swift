//
//  RoundedButton.swift
//  LeRobot
//
//  Created by developer on 29/05/21.
//

import UIKit

protocol RoundedButtonEventDelegate: AnyObject {
    func buttonPressed(property: RoundedButton.Property)
}

final class RoundedButton: UIButton {

    enum Property: String {
        case classifier, teach, erase
        var icon: UIImage? { UIImage(named: "\(rawValue)-button-image") }
        var name: String { rawValue.capitalizeFirstLetter() }
    }

    private struct Constant {
        static let defaultFont = UIFont(name: "HelveticaNeue", size: 14)
        static let cornerRadius: CGFloat = 15
        static let borderWidth: CGFloat = 1
        static let padding: CGFloat = 1
    }

    let property: Property
    private weak var delegate: RoundedButtonEventDelegate?

    init(appearance: Property) {
        self.property = appearance
        super.init(frame: CGRect(x: .zero, y: .zero, width: .zero, height: .zero))
        addTarget(self, action: #selector(onPressed), for: .touchUpInside)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setDelegate(delegate: RoundedButtonEventDelegate?) {
        self.delegate = delegate
    }

    func setupLayoutAndAppearance() {
        setImage(property.icon, for: .normal)
        setTitle(property.name, for: .normal)
        setTitleColor(.wine, for: .normal)
        titleLabel?.font = Constant.defaultFont
        backgroundColor = .maxRose
        layer.cornerRadius = Constant.cornerRadius
        layer.borderColor = UIColor.wine.cgColor
        layer.borderWidth = Constant.borderWidth
        alignImageAndTitleVertically()
    }

    @objc private func onPressed(sender: UIButton) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.delegate?.buttonPressed(property: self.property)
        }
    }

    private func alignImageAndTitleVertically(padding: CGFloat = Constant.padding) {
        guard let imageSize = imageView?.frame.size,
              let titleSize = titleLabel?.frame.size else { return }

        let totalHeight = imageSize.height + titleSize.height + padding

        imageEdgeInsets = UIEdgeInsets(
            top: -(totalHeight - imageSize.height),
            left: .zero,
            bottom: .zero,
            right: -titleSize.width
        )

        titleEdgeInsets = UIEdgeInsets(
            top: .zero,
            left: -imageSize.width,
            bottom: -(totalHeight - titleSize.height),
            right: .zero
        )
    }
}
