//
//  HandwritingInteractorFactory.swift
//  LeRobot
//
//  Created by developer on 04/06/21.
//

import UIKit

final class HandwritingInteractorFactory {
    static func create(viewController: HandwritingViewControllerPresentable) -> HandwritingInteractorType {
        HandwritingInteractor(HandwritingPresenter(viewController: viewController),
                              HandwritingApiWorker(),
                              ImageProcessor())
    }
}
