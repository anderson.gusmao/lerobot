//
//  HandwritingInteractor.swift
//  LeRobot
//
//  Created by developer on 03/06/21.
//

import UIKit

protocol HandwritingInteractorType {
    func classify(image: UIImage)
    func teach(image: UIImage, text: String)
}

final class HandwritingInteractor: HandwritingInteractorType {

    private let presenter: HandwritingPresenterType
    private let worker: HandwritingApiWorkerType
    private let imageProcessor: ImageProcessorType

    init(_ presenter: HandwritingPresenterType,
         _ worker: HandwritingApiWorkerType,
         _ imageProcessor: ImageProcessorType) {
        self.presenter = presenter
        self.worker = worker
        self.imageProcessor = imageProcessor
    }

    func classify(image: UIImage) {
        let imageArray = imageProcessor.convertIntoArray(image)
        worker.classify(input: HandwritingClassificationRequest(info: imageArray)) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                self.presenter.onClassificationProcessed(result: response.result)
            case .failure(let error):
                self.presenter.onError(error: error)
            }
        }
    }

    func teach(image: UIImage, text: String) {
        let imageArray = imageProcessor.convertIntoArray(image)
        worker.teach(input: HandwritingTeachingRequest(info: imageArray, label: text)) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success():
                self.presenter.onTeachingProcessed()
            case .failure(let error):
                self.presenter.onError(error: error)
            }
        }
    }
}
