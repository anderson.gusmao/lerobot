//
//  HandwritingClassificationRequest.swift
//  LeRobot
//
//  Created by developer on 04/06/21.
//

import Foundation

struct HandwritingClassificationRequest: Codable {
    let info: [Int]
}
