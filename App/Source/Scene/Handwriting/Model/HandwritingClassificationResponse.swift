//
//  HandwritingClassificationResponse.swift
//  LeRobot
//
//  Created by developer on 04/06/21.
//

import Foundation

struct HandwritingClassificationResponse: Decodable {
    let result: String
}
