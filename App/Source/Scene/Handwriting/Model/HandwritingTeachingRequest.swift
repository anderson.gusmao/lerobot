//
//  HandwritingTeachingRequest.swift
//  LeRobot
//
//  Created by developer on 04/06/21.
//

import Foundation

struct HandwritingTeachingRequest: Codable {
    let info: [Int]
    let label: String
}
