//
//  HandwritingPresenter.swift
//  LeRobot
//
//  Created by developer on 03/06/21.
//

import Foundation

protocol HandwritingViewControllerPresentable: AnyObject {
    func presentClassificationResult(text: String)
    func presentServiceResult(error: Error?)
}

protocol HandwritingPresenterType: AnyObject {
    func onClassificationProcessed(result: String)
    func onTeachingProcessed()
    func onError(error: Error)
}

final class HandwritingPresenter: HandwritingPresenterType {

    weak var viewController: HandwritingViewControllerPresentable?

    init(viewController: HandwritingViewControllerPresentable) {
        self.viewController = viewController
    }

    func onClassificationProcessed(result: String) {
        DispatchQueue.main.async { [weak self] in
            self?.viewController?.presentClassificationResult(text: result)
        }
    }

    func onTeachingProcessed() {
        DispatchQueue.main.async { [weak self] in
            self?.viewController?.presentServiceResult(error: nil)
        }
    }

    func onError(error: Error) {
        DispatchQueue.main.async { [weak self] in
            self?.viewController?.presentServiceResult(error: error)
        }
    }
}
