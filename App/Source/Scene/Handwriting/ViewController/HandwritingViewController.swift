//
//  ViewController.swift
//  LeRobot
//
//  Created by developer on 27/05/21.
//

import UIKit

final class HandwritingViewController: UIViewController, HandwritingViewControllerPresentable {

    private struct Constant {
        static let errorTitle = "Error"
        static let inputTitle = "Teach"
        static let inputMessage = "Type a letter here..."
        static let okayButton = "OK"
        static let canvas = Canvas()
    }
    private struct Canvas {
        let size = UIScreen.main.bounds.width - 40
    }

    private var drawingView = DrawingView(size: CGSize(width: Constant.canvas.size,
                                                       height: Constant.canvas.size))
    private let buttons: [RoundedButton]
    private let buttonContainer: ButtonContainer
    private let displayView = DisplayView()
    private let loadingView = LoadingView()
    private let footerView = FooterView()
    private let mainHeadingText = HeadingText(text: "Draw a character here...")
    private var interactor: HandwritingInteractorType?

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    init() {
        buttons = [RoundedButton(appearance: .classifier),
                   RoundedButton(appearance: .teach),
                   RoundedButton(appearance: .erase)]
        buttonContainer = ButtonContainer(buttons: buttons)
        super.init(nibName: nil, bundle: nil)
        buttons.forEach({ $0.setDelegate(delegate: self) })
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        buildViews()
        buildInteractor()
    }

    func presentClassificationResult(text: String) {
        footerView.displayView.setValue(text)
        hideLoadingView()
    }

    func presentServiceResult(error: Error? = nil) {
        hideLoadingView()
        guard let error = error else { return }

        let alert = UIAlertController(title: Constant.errorTitle,
                                      message: error.localizedDescription,
                                      preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: Constant.okayButton, style: UIAlertAction.Style.default, handler: nil))
         present(alert, animated: true, completion: nil)
    }
}

extension HandwritingViewController: RoundedButtonEventDelegate {

    func buttonPressed(property: RoundedButton.Property) {
        switch property {
        case .classifier:
            guard let drawing = drawingView.image else { return }
            showLoadingView()
            interactor?.classify(image: drawing)
        case .teach:
            guard let drawing = drawingView.image else { return }
            showInputText { [weak self] text in
                guard let self = self, !text.isEmpty else { return }
                self.showLoadingView()
                self.interactor?.teach(image: drawing, text: text)
            }

        case .erase:
            footerView.displayView.clean()
            drawingView.eraseImage()
        }
    }
}

private extension HandwritingViewController {

    func showInputText(completionHandler: @escaping (String) -> Void) {
        let alert = UIAlertController(title: Constant.inputTitle,
                                      message: Constant.inputMessage,
                                      preferredStyle: .alert)
        alert.addTextField { (textField) in textField.text = .empty }
        alert.addAction(UIAlertAction(title: Constant.okayButton,
                                      style: .default,
                                      handler: { [weak alert] _ in
            guard let textField = alert?.textFields?[0] else { return }
                                        completionHandler(textField.text ?? .empty)
        }))
        present(alert, animated: true, completion: nil)
    }

    func showLoadingView() {
        view.addSubview(loadingView)
        loadingView.applyConstraintsTo(view)
    }

    func hideLoadingView() {
        loadingView.removeFromSuperview()
    }

    func buildInteractor() {
        interactor = HandwritingInteractorFactory.create(viewController: self)
    }

    func buildViews() {
        addMainView()
        addMainHeadingText()
        addDrawingView()
        addButtonContainerView()
        addFooterView()
    }

    func addMainHeadingText() {
        view.addSubview(mainHeadingText)
        mainHeadingText.applyConstraintTo(view)
    }

    func addFooterView() {
        view.addSubview(footerView)
        footerView.applyConstraintTo(view, yAxisAnchor: buttonContainer.bottomAnchor)
    }

    func addButtonContainerView() {
        view.addSubview(buttonContainer)
        buttonContainer.applyConstraintTo(view, yAxisAnchor: drawingView.bottomAnchor)
    }

    func addMainView() {
        view.backgroundColor = .softRose
    }

    func addDrawingView() {
        view.addSubview(drawingView)
        drawingView.applyConstraintTo(xAxisAnchor: view.centerXAnchor, yAxisAnchor: mainHeadingText.bottomAnchor)
    }
}
