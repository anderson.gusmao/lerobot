//
//  HandwritingApiWorker.swift
//  LeRobot
//
//  Created by developer on 04/06/21.
//

import Foundation

typealias ClassifyCompletionHandler = (Result<HandwritingClassificationResponse, NetworkError>) -> Void
typealias TeachingCompletionHandler = (Result<Void, NetworkError>) -> Void

protocol HandwritingApiWorkerType {
    func classify(input: HandwritingClassificationRequest,
                  callback: @escaping ClassifyCompletionHandler)
    func teach(input: HandwritingTeachingRequest, callback: @escaping TeachingCompletionHandler)
}

enum NetworkError: Error { case unknown(String) }
enum HttpMethod: String { case post, put }

final class HandwritingApiWorker: HandwritingApiWorkerType {

    private var task: URLSessionDataTask?

    private struct Constant {
        static let contentTypeHeader = "Content-Type"
        static let contentTypeValue = "application/json"
        static let encodingError = "application/json"
    }

    func classify(input: HandwritingClassificationRequest,
                  callback: @escaping ClassifyCompletionHandler) {
        task?.cancel()

        guard let postData = createPostData(requestBody: input) else {
            callback(.failure(.unknown(Constant.encodingError)))
            return
        }

        let request = createRequest(postData, method: .post)

        task = URLSession.shared.dataTask(with: request) { data, response, error in

            guard error == nil, let data = data,
                  let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200,
                  let result = try? JSONDecoder().decode(HandwritingClassificationResponse.self, from: data) else {
                callback(.failure(.unknown(error.debugDescription)))
                return
            }
            callback(.success(result))
        }
        task?.resume()
    }

    func teach(input: HandwritingTeachingRequest,
               callback: @escaping TeachingCompletionHandler) {
        task?.cancel()

        guard let postData = createPostData(requestBody: input) else {
            callback(.failure(.unknown(Constant.encodingError)))
            return
        }

        let request = createRequest(postData, method: .put)

        task = URLSession.shared.dataTask(with: request) { _, response, error in

            guard error == nil, let httpResponse = response as? HTTPURLResponse,
                  httpResponse.statusCode == 201 else {
                callback(.failure(.unknown(error.debugDescription)))
                return
            }
            callback(.success(()))
        }
        task?.resume()
    }

    private func createRequest(_ postData: String, method: HttpMethod) -> URLRequest {
        var request = URLRequest(url: Config.url)
        request.addValue(Constant.contentTypeValue, forHTTPHeaderField: Constant.contentTypeHeader)
        request.httpMethod = method.rawValue.uppercased()
        request.httpBody = postData.data(using: .utf8)
        return request
    }

    private func createPostData<T: Codable>(requestBody: T) -> String? {
        guard let data = try? JSONEncoder().encode(requestBody),
              let postData = String(data: data, encoding: .utf8) else {
            return nil
        }
        return postData
    }
}
