//
//  Config.swift
//  LeRobot
//
//  Created by developer on 04/06/21.
//

import Foundation

final class Config {

    static var url: URL {
        guard let path = Bundle.main.path(forResource: "Info", ofType: "plist"),
              let dic = NSDictionary(contentsOfFile: path),
              let urlValue = dic["URL_API"] as? String,
              let url = URL(string: urlValue) else {
            fatalError("Could not locate url configuration.")
        }
        return url
    }
}
