//
//  ImageProcessor.swift
//  LeRobot
//
//  Created by developer on 04/06/21.
//

import Foundation
import UIKit

protocol ImageProcessorType {
    func convertIntoArray(_ image: UIImage) -> [Int]
}

final class ImageProcessor: ImageProcessorType {

    private let targetSize: CGSize

    init(targetSize: CGSize = CGSize(width: 32, height: 32)) {
        self.targetSize = targetSize
    }

    func convertIntoArray(_ image: UIImage) -> [Int] {
        let imageResized = resizeImage(image)
        return imageToArray(imageResized)
    }

    private func resizeImage(_ image: UIImage) -> UIImage {
        let rect = CGRect(x: .zero, y: .zero, width: targetSize.width, height: targetSize.height)
        UIGraphicsBeginImageContextWithOptions(targetSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext() ?? image
        UIGraphicsEndImageContext()
        return newImage
    }

    private func imageToArray(_ image: UIImage) -> [Int] {

        var result = [Int]()

        for row in .zero..<Int(image.size.height) {
            for col in .zero..<Int(image.size.width) {
                let state = image.cgImage?.pixelFrom(x: col, y: row)?.alpha ?? .zero > .zero
                result.append(state ? 1 : .zero)
            }
        }
        return result
    }
}
