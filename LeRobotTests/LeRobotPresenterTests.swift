//
//  LeRobotTests.swift
//  LeRobotTests
//
//  Created by developer on 27/05/21.
//

import XCTest
@testable import LeRobot

class LeRobotPresenterTests: XCTestCase {

    func testPresentClassificationResult() throws {
        let presentExpectation = expectation(description: "presentClassificationResult")

        let presentableMock = PresentableMock(presentClassificationResultExpectation: presentExpectation,
                                              presentServiceResultExpectation: nil,
                                              presentServiceResultErrorExpectation: nil)

        let interactor = HandwritingInteractor(HandwritingPresenter(viewController: presentableMock),
                                               HandwritingApiWorkerMock(),
                                               ImageProcessor())
        interactor.classify(image: UIImage())
        waitForExpectations(timeout: 0.3, handler: nil)
    }

    func testPresentErrorResult() throws {
        let presentErrorExpectation = expectation(description: "presentServiceResultErrorExpectation")

        let presentableMock = PresentableMock(presentClassificationResultExpectation: nil,
                                              presentServiceResultExpectation: nil,
                                              presentServiceResultErrorExpectation: presentErrorExpectation)

        let interactor = HandwritingInteractor(HandwritingPresenter(viewController: presentableMock),
                                               HandwritingApiWorkerMock(),
                                               ImageProcessorErrorMock())
        interactor.classify(image: UIImage())
        waitForExpectations(timeout: 0.3, handler: nil)
    }

    func testPresentResult() throws {
        let presentResultExpectation = expectation(description: "presentServiceResultExpectation")

        let presentableMock = PresentableMock(presentClassificationResultExpectation: nil,
                                              presentServiceResultExpectation: presentResultExpectation,
                                              presentServiceResultErrorExpectation: nil)

        let interactor = HandwritingInteractor(HandwritingPresenter(viewController: presentableMock),
                                               HandwritingApiWorkerMock(),
                                               ImageProcessor())
        interactor.teach(image: UIImage(), text: "None")
        waitForExpectations(timeout: 0.3, handler: nil)
    }
}
