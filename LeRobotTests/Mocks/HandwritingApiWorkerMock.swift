//
//  HandwritingApiWorkerMock.swift
//  LeRobotTests
//
//  Created by developer on 06/06/21.
//

import Foundation
@testable import LeRobot

final class HandwritingApiWorkerMock: HandwritingApiWorkerType {
    func classify(input: HandwritingClassificationRequest, callback: @escaping ClassifyCompletionHandler) {

        guard input.info.count > 0,
              let data = "{\"result\": \"1\"}".data(using: .utf8),
              let response = try? JSONDecoder().decode(HandwritingClassificationResponse.self, from: data) else {
            callback(.failure(.unknown("General error!")))
            return
        }

        callback(.success(response))
    }

    func teach(input: HandwritingTeachingRequest, callback: @escaping TeachingCompletionHandler) {
        guard input.info.count > 0 else {
            callback(.failure(.unknown("General error!")))
            return
        }

        callback(.success(()))
    }
}
