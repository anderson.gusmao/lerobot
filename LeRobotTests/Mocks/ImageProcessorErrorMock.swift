//
//  ImageProcessorErrorMock.swift
//  LeRobotTests
//
//  Created by developer on 06/06/21.
//

@testable import LeRobot
import UIKit

final class ImageProcessorErrorMock: ImageProcessorType {
    func convertIntoArray(_ image: UIImage) -> [Int] {
        return []
    }
}
