//
//  PresentableMock.swift
//  LeRobotTests
//
//  Created by developer on 06/06/21.
//

import XCTest
@testable import LeRobot

final class PresentableMock: HandwritingViewControllerPresentable {

    private let presentClassificationResultExpectation: XCTestExpectation?
    private let presentServiceResultExpectation: XCTestExpectation?
    private let presentServiceResultErrorExpectation: XCTestExpectation?

    init(presentClassificationResultExpectation: XCTestExpectation?,
         presentServiceResultExpectation: XCTestExpectation?,
         presentServiceResultErrorExpectation: XCTestExpectation?) {
        self.presentClassificationResultExpectation = presentClassificationResultExpectation
        self.presentServiceResultExpectation = presentServiceResultExpectation
        self.presentServiceResultErrorExpectation = presentServiceResultErrorExpectation
    }

    func presentClassificationResult(text: String) {
        presentClassificationResultExpectation?.fulfill()
    }

    func presentServiceResult(error: Error?) {
        guard error == nil else {
            presentServiceResultErrorExpectation?.fulfill()
            return
        }
        presentServiceResultExpectation?.fulfill()
    }
}
