# What is this?
The **Le Robot project** is an iOS client for the [Grave project](https://gitlab.com/mascate/grave) which is a machine learning project for handwriting recognition. In this way, this app just converts your drawing on the canvas into JSON format to API requests in the **Grave Project**. By saying **"just converts"**, it means the app has no machine learning code to identify what is being drawn on the canvas. All machine learning code is in the [Grave project](https://gitlab.com/mascate/grave).

# See a demo

This is how **Le Robot** behaves, as you draw a character on the canvas, you can request a classification for that. If the Grave API can recognize it, it will suggest a label for that image, and as it can be wrong based on stored samples calligraphy, you can teach it, **add two or three samples using your handwriting style** and the Grave API will be able to recognize your drawing.

**Important note**

The base of the [Grave project](https://gitlab.com/mascate/grave) **contains only samples of handwriting numbers**, but you can teach it with letters as well, so you can use alphabet handwriting recognition.

![alt Le Robot demo](/Readme/lerobot.demo.gif)

# Curious about how the machine learning project works? 
* Take a look at the [Grave project](https://gitlab.com/mascate/grave).

# How to test it
1. Clone, build, and run the [Grave project](https://gitlab.com/mascate/grave), in your local machine or publish it on any cloud service of your preference using the docker image file.
2. Clone this project on your computer: `git clone git@gitlab.com:anderson.gusmao/lerobot.git`
3. Access the project directory and install pods: `pod install` *Make sure you have cocoa pods installed on your computer*
4. Open the workspace file, go to project settings, and on the **Build Settings** section, change the URL according to your configuration. *Take a look on the image bellow*
5. Run and test it.

![alt Le Robot demo](/Readme/configure.url.png)

# How it works

As the user draws any character on the canvas and taps on classify button, the app logic resizes the image into **32x32 pixels** to optimize processing and converts the image into an array representation using integers i.e. **zeros and ones** to be sent via API to the [Grave project](https://gitlab.com/mascate/grave)

![alt Le Robot demo](/Readme/howitworks.png)

**Image representation demo:**

![alt Le Robot demo](/Readme/array.representation.png)

# About Le Robot

**Here are some characteristics of this project:**

* Clean swift architecture;
* Applied Swift Lint via cocoa pods;
* UIKit project;
* Did you find some improvement opportunities? Please make an MR, let's make it better;
